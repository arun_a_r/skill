local m = require "matrix"

a = m(6,6,1)
print(a)
print(#a)
while #a ~= 0 do rawset(a, #a, nil) end
print(#a)


x = m(1,6,1)
print(x)
y = m(1,2,0)
x = m.concath(x,y)
print(x)
