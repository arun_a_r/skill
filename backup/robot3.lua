msg = {}
msg[1] = "Help"
msg[2] = "Willing"
msg[3] = "ACK_initiator"
msg[4] = "ACK_non-initator"
msg[5] = "beacon"
msg[6] = "grabbed road-blocker"
msg[7] = "go north"
msg[8] = "go west"
msg[9] = "go south"
msg[10] = "go east"
msg[11] = "home orientation set"
msg[12] = "let's go"
--------------------------------------------------------------------------------
l = 0
r = 0
direction = 0
count_time = 0
count_time = 0
go_ahead = 0
--------------------------------------------------------------------------------
local m = require "matrix"
bat_total = 10000
bat_cur = 10000
grab_timer = 0
es = 0
object = "none"
--------------------------------------------------------------------------------
rs_small_box = m{{1,1,0,1,0,0}}
rs_large_box = m{{1,1,1,1,1,0}}
rs_small_disc = m{{1,0,0,0,0,1}}
rs_large_disc = m{{1,0,1,0,1,0}}
required_skills = m(1,6,0)
chk_skill = m(1,6,0)
empty_skill = m(1,8,0)
--------------------------------------------------------------------------------
my_skill = m{{0,0,0,1,0,1,50,20}}
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
function check_skill()
  if #robot.colored_blob_omnidirectional_camera ~= 0  then
      for i = 1,#robot.colored_blob_omnidirectional_camera do
          if (robot.colored_blob_omnidirectional_camera[i].color.red == 165 and
              robot.colored_blob_omnidirectional_camera[i].color.green == 42 and
              robot.colored_blob_omnidirectional_camera[i].color.blue == 42)  then
                  chk_skill = rs_large_disc
          elseif(robot.colored_blob_omnidirectional_camera[i].color.red == 255 and
              robot.colored_blob_omnidirectional_camera[i].color.green == 255 and
              robot.colored_blob_omnidirectional_camera[i].color.blue == 255) then
                  chk_skill =  rs_small_disc
          elseif(robot.colored_blob_omnidirectional_camera[i].color.red == 160 and
              robot.colored_blob_omnidirectional_camera[i].color.green == 32 and
              robot.colored_blob_omnidirectional_camera[i].color.blue == 240) then
                  chk_skill = rs_small_box
          elseif(robot.colored_blob_omnidirectional_camera[i].color.red == 255 and
              robot.colored_blob_omnidirectional_camera[i].color.green == 140 and
              robot.colored_blob_omnidirectional_camera[i].color.blue == 0) then
                  chk_skill = rs_small_box
          end
      end
  end
  chk_flag = 0
  for i = 1, 6 do
    if chk_skill[1][i] == my_skill[1][i] then
      check_flag = 1
    end
  end
  if #robot.colored_blob_omnidirectional_camera ~= 0  then
      for i = 1,#robot.colored_blob_omnidirectional_camera do
        if (robot.colored_blob_omnidirectional_camera[i].color.red == 0 and
            robot.colored_blob_omnidirectional_camera[i].color.green == 0 and
            robot.colored_blob_omnidirectional_camera[i].color.blue == 255) or
          (robot.colored_blob_omnidirectional_camera[i].color.red == 255 and
            robot.colored_blob_omnidirectional_camera[i].color.green == 0 and
            robot.colored_blob_omnidirectional_camera[i].color.blue == 255) or
          (robot.colored_blob_omnidirectional_camera[i].color.red == 255 and
            robot.colored_blob_omnidirectional_camera[i].color.green == 0 and
            robot.colored_blob_omnidirectional_camera[i].color.blue == 0) or
          (robot.colored_blob_omnidirectional_camera[i].color.red == 255 and
            robot.colored_blob_omnidirectional_camera[i].color.green == 255 and
            robot.colored_blob_omnidirectional_camera[i].color.blue == 0) or
            (robot.colored_blob_omnidirectional_camera[i].color.red == 0 and
            robot.colored_blob_omnidirectional_camera[i].color.green == 255 and
            robot.colored_blob_omnidirectional_camera[i].color.blue == 255) or
            (robot.colored_blob_omnidirectional_camera[i].color.red == 0 and
            robot.colored_blob_omnidirectional_camera[i].color.green == 255 and
            robot.colored_blob_omnidirectional_camera[i].color.blue == 0) then
              check_flag = 0
          end
        end
      end
  return(check_flag)
end
-----------------function compose()---------------------------------------------
function compose(to_addr,meaning,skill)
  message = {self_addr,to_addr,meaning}
  for i = 1, 8 do
    table.insert(message, skill[1][i])
  end
  return(message)
end
--------------------------------------------------------------------------------
function init()
  self_addr = addr(robot.id)
  log(robot.id," = ",id)
  state = "search"
  prev_state = "dummy"
  int_state = "listening"
  prev_int_state = "dummy"
  robot.colored_blob_omnidirectional_camera.enable()
end

function step()
    --colors
--[[    if (bat_cur/bat_total) > 0.25 then
        robot.leds.set_all_colors(0,0,255)
    else
        robot.leds.set_all_colors(255,0,0)
    end]]--
    if state ~= prev_state then
        log(self_addr,"=",state)
    end
    prev_state = state
    if state == "search" then
      robot.leds.set_all_colors(0,0,0)
        search()
    elseif state == "choose" then
      robot.leds.set_all_colors(255,0,0)
        choose()
    elseif state == "approach" then
      robot.leds.set_all_colors(255,0,0)
        approach()
    elseif state == "grab" then
      robot.leds.set_all_colors(255,0,0)
      grab()
    elseif state == "decide" then
      robot.leds.set_all_colors(255,0,0)
        decide()
    elseif state == "call" then
      robot.leds.set_all_colors(255,0,0)
      call()
    elseif state == "specific_call" then
      robot.leds.set_all_colors(255,0,0)
      specific_call()
    elseif state == "waiting_for_final_ack" then
      robot.leds.set_all_colors(255,0,0)
      waiting_for_final_ack()
    elseif state == "waiting" then
      robot.leds.set_all_colors(255,0,0)
      waiting()
    elseif state == "determine_orient" then
      robot.leds.set_all_colors(255,0,0)
        determine_orient()
    elseif state == "go_home" then
      robot.leds.set_all_colors(255,0,0)
        go_home()
    elseif state == "home" then
      robot.leds.set_all_colors(255,0,0)
        home()
    elseif state == "dest_orient_determine" then
      robot.leds.set_all_colors(255,0,0)
      dest_orient_determine()
    end
end

--------------------------------------------------------------------------------
function reset()
end
function destroy()
    log('Energy spent by ',robot.id,' / ',id,' = ',es,'\n')
end
--------------------------------------------------------------------------------
----------------------------------addr fn----------------------    log(self_addr," sending ",msg[2]," to ",caller.data[1])
-----------------
-----------A hash function that decides the address of the robot----------------
--------------------------------------------------------------------------------
function addr(s)
    i = 0
    id = 0
    for c in s:gmatch"." do
        id = id + (string.byte(c) * math.pow(2 , i))
        i = i + 1
    end
    id = math.fmod(id,251) + 1
    return id
end
--------------------------------------------------------------------------------
-----------------------------function search()----------------------------------
--------------------------------------------------------------------------------
function search()
    --bat_cur = bat_cur - 10
    if int_state ~= prev_int_state then
        log(self_addr,"=",int_state)
    end
    prev_int_state = int_state
    es = es + 10
    if int_state == "listening" then
      sensingLeft =     robot.proximity[3].value +
                        robot.proximity[4].value +
                        robot.proximity[5].value +
                        robot.proximity[6].value +
                        robot.proximity[2].value +
                        robot.proximity[1].value

      sensingRight =    robot.proximity[19].value +
                        robot.proximity[20].value +
                        robot.proximity[21].value +
                        robot.proximity[22].value +
                        robot.proximity[24].value +
                        robot.proximity[23].value
      --[[if bat_cur <= (30/100) * bat_total then
          state = "to_charge"
      end]]
      if sensingLeft ~= 0 then
            robot.wheels.set_velocity(7,3)
      elseif sensingRight ~= 0 then
            robot.wheels.set_velocity(3,7)
      else
            robot.wheels.set_velocity(10,10)
      end
      if #robot.colored_blob_omnidirectional_camera ~= 0  then
          for i = 1,#robot.colored_blob_omnidirectional_camera do
              check_flag = check_skill()
              if (robot.colored_blob_omnidirectional_camera[i].color.red == 165 and
                  robot.colored_blob_omnidirectional_camera[i].color.green == 42 and
                  robot.colored_blob_omnidirectional_camera[i].color.blue == 42) and check_flag == 1  then
                      state = "choose"
                      object = "large_disc"
                      log(object)
              elseif(robot.colored_blob_omnidirectional_camera[i].color.red == 255 and
                  robot.colored_blob_omnidirectional_camera[i].color.green == 255 and
                  robot.colored_blob_omnidirectional_camera[i].color.blue == 255) and check_flag == 1 then
                      state = "choose"
                      object = "small_disc"
                      log(object)
              elseif(robot.colored_blob_omnidirectional_camera[i].color.red == 160 and
                  robot.colored_blob_omnidirectional_camera[i].color.green == 32 and
                  robot.colored_blob_omnidirectional_camera[i].color.blue == 240) and check_flag == 1 then
                      state = "choose"
                      object = "small_box"
                      log(object)
              elseif(robot.colored_blob_omnidirectional_camera[i].color.red == 255 and
                  robot.colored_blob_omnidirectional_camera[i].color.green == 140 and
                  robot.colored_blob_omnidirectional_camera[i].color.blue == 0) and check_flag == 1 then
                      state = "choose"
                      object = "large_box"
                      log(object)
              end
          end
      end
      calls = {}
      if #robot.range_and_bearing > 0 then
          for i = 1,#robot.range_and_bearing do
              if robot.range_and_bearing[i].data[2] == 255 then
                  table.insert(calls, robot.range_and_bearing[i])
              end
          end
      end
      if #calls > 0 then
          distance = 10000
          caller = 0
          for i = 1, #calls do
              if calls[i].range < distance then
                  distance = calls[i].range
                  caller = calls[i]
              end
          end
      end
      if #calls > 0 and caller ~= 0 then
        int_state = "responding_to_braodcast"
      end
  elseif int_state == "responding_to_braodcast" then
      responding_to_braodcast()
  elseif int_state == "waiting_for_final_call" then
      waiting_for_final_call()
  elseif int_state == "give_final_ack" then
      give_final_ack()
  elseif int_state == "orient" then
      orient()
  elseif int_state == "approach_caller" then
      approach_caller()
  elseif int_state == "grab_robot" then
    robot.leds.set_all_colors(255,0,0)
      grab_robot()
  elseif int_state == "final_orientation" then
    robot.leds.set_all_colors(255,0,0)
      final_orientation()
  elseif int_state == "home" then
    robot.leds.set_all_colors(255,0,0)
      home()
  end
end

--------------------------------------------------------------------------------
--------------------------------to charge()-------------------------------------
--------------------------------------------------------------------------------
--[[function to_charge()
    if robot.positioning.position.x < 0 then -- the location we must go

    elseif robot.positioning.position.x >= 0 then

    end

end--]]
--[[----------------------------------------------------------------------------
-----------------------------function charge()----------------------------------
--------------------------------------------------------------------------------
function to_charge()
    robot.wheels.set_velocity(0,0)
    bat_cur = bat_cur + 10
    if bat_cur >= (90/100)*bat_total then
        state = "search"
    end
end
--------------------------------------------------------------------------------
]]--
----------------------------function choose()-----------------------------------
function choose()
  if #robot.colored_blob_omnidirectional_camera == 0 then
      state = "search"
      --log(1)
  else
      --log(2)
      robot.wheels.set_velocity(0,0)
      closest = robot.colored_blob_omnidirectional_camera[1]
      dist = robot.colored_blob_omnidirectional_camera[1].distance
      ang =  robot.colored_blob_omnidirectional_camera[1].angle
      for i = 1, #robot.colored_blob_omnidirectional_camera do

          if (robot.colored_blob_omnidirectional_camera[i].color.red == 165 and
              robot.colored_blob_omnidirectional_camera[i].color.green == 42 and
              robot.colored_blob_omnidirectional_camera[i].color.blue == 42) or
              (robot.colored_blob_omnidirectional_camera[i].color.red == 255 and
               robot.colored_blob_omnidirectional_camera[i].color.green == 255 and
               robot.colored_blob_omnidirectional_camera[i].color.blue == 255) or
               (robot.colored_blob_omnidirectional_camera[i].color.red == 160 and
                robot.colored_blob_omnidirectional_camera[i].color.green == 32 and
                robot.colored_blob_omnidirectional_camera[i].color.blue == 240) or
                (robot.colored_blob_omnidirectional_camera[i].color.red == 255 and
                robot.colored_blob_omnidirectional_camera[i].color.green == 140 and
                robot.colored_blob_omnidirectional_camera[i].color.blue == 0)then
              closest = robot.colored_blob_omnidirectional_camera[i]
              dist = closest.distance
              ang = closest.angle

          end

      end
      if ang > 0.1 then
          robot.wheels.set_velocity(-1,1)
      elseif ang < -0.1 then
          robot.wheels.set_velocity(1,-1)
      elseif ang >= -0.1 and ang <= 0.1 then
          state = "approach"
      end
  end
end
---------------------------function approach()----------------------------------
function approach()
    if #robot.colored_blob_omnidirectional_camera > 0 then
        x = 0
        for i = 1, 24 do --some modification must be done here as we need not check
                         --all proximity sensors then ones located in front shall do
            if x < robot.proximity[i].value then
                x = robot.proximity[i].value
            end
        end
-------trying to keep the orientation while approaching the obstacle------------
        dist = robot.colored_blob_omnidirectional_camera[1].distance
        ang =  robot.colored_blob_omnidirectional_camera[1].angle

        for i = 1, #robot.colored_blob_omnidirectional_camera do
            if dist > robot.colored_blob_omnidirectional_camera[i].distance and
                (robot.colored_blob_omnidirectional_camera[i].color.red == 255 or
                robot.colored_blob_omnidirectional_camera[i].color.blue == 255) then

                dist = robot.colored_blob_omnidirectional_camera[i].distance
                ang = robot.colored_blob_omnidirectional_camera[i].angle

            end
        end
        if ang > 0 then
            robot.wheels.set_velocity(5,6)
        end
        if ang < 0 then
            robot.wheels.set_velocity(6,5)
        end
        if ang == 0 then
            robot.wheels.set_velocity(5,5)
        end
-------------trying to slow down when reaching near the obstacle----------------
        if x >= 0.5 then
            robot.wheels.set_velocity((1 - x) * 10, (1 - x) * 10)
        end
        if x >= 0.9 then
            robot.wheels.set_velocity(0,0)
            state = "grab"
        end
    else
        state = "search"
    end
end
---------------------grab function--
function grab()
  --log(grab_timer)
  grab_timer = grab_timer + 1
    grip_ang = 200
    x = robot.proximity[1]
    x.value = 0
    pos = 0
    for i = 1,24 do
        if robot.proximity[i].value >= x.value then
            x = robot.proximity[i]
            pos = i
        end
    end
    if x.value >= 0.92 then
        grip_ang = x.angle
    elseif pos >= 1 and pos <= 12 then
        robot.wheels.set_velocity(0,0.75)
    elseif pos >= 13 and pos <= 24 then
        robot.wheels.set_velocity(0.75,0)
    end
    if grip_ang ~= 200 then
        --log(pos," angle: ",x.angle,grip_ang,"value: ",robot.proximity[pos].value)
        robot.wheels.set_velocity(0,0)
        robot.turret.set_rotation(grip_ang)
        robot.gripper.lock_negative()
        count_time = count_time + 1
    end
    if grab_timer >= 50 then
        robot.gripper.lock_negative()
        robot.turret.set_passive_mode()
        grab_timer = 0
        state = "decide"
    end
end
----------------------------function decide()-----------------------------------
function decide()
  log(object)
  if object == "small_box" then
    required_skills = rs_small_box
  elseif object == "large_box" then
    required_skills = rs_large_box
  elseif object == "small_disc" then
    required_skills = rs_small_disc
  elseif object == "large_disc" then
    required_skills = rs_large_box
  else
    state = "search"
  end
  print(required_skills)
  for i = 1, 6 do
    if required_skills[1][i] ~= 0 then
      required_skills[1][i] = required_skills[1][i] + 1
    end
  end
  for i = 1, 6 do
    if my_skill[1][i] == 1 then
      required_skills[1][i] = required_skills[1][i] - 1
    end
  end
  print(required_skills)
  state = "call"
end
--------------------------------------------------------------------------------
function call()
  add_skills = compose(255,1,required_skills)
  robot.range_and_bearing.set_data(add_skills)
  distance = 10000
  nearest = 0
  if #robot.range_and_bearing > 0 then
      for i = 1, #robot.range_and_bearing do
          if robot.range_and_bearing[i].data[2] == self_addr and
              robot.range_and_bearing[i].data[3] == 2 and
              robot.range_and_bearing[i].range < distance then
              distance = robot.range_and_bearing[i].range
              nearest = robot.range_and_bearing[i]
          end
      end
  end
  if nearest ~= 0 then
      state = "specific_call"
  end
end
--------------------------------------------------------------------------------
function specific_call()
    specific_msg = compose(nearest.data[1],3,empty_skill)
    robot.range_and_bearing.set_data(specific_msg)
    log(self_addr," sending ",msg[3]," to ",nearest.data[1])
    state = "waiting_for_final_ack"
end
---------------------Function waiting_for_final_ack-----------------------------
--------------------------------------------------------------------------------
function waiting_for_final_ack()
    if #robot.range_and_bearing > 0 then
        for i = 1, #robot.range_and_bearing do
            if robot.range_and_bearing[i].data[3] == 4 then
                log(self_addr,"received final ack from",robot.range_and_bearing[i].data[1])
                state = "waiting"
            end
        end
    end
end
------------------------------Function waiting----------------------------------
--------------------------------------------------------------------------------
function waiting()
    robot.wheels.set_velocity(0,0)
    beacon = compose(nearest.data[1],5,empty_skill)
    robot.range_and_bearing.set_data(beacon)
    if #robot.range_and_bearing > 0 then

        for i = 1,#robot.range_and_bearing do

            if robot.range_and_bearing[i].data[2] == self_addr and
                robot.range_and_bearing[i].data[3] == 6 then
                state = "dest_orient_determine"
                log (self_addr," received ",msg[robot.range_and_bearing[i].data[3]]," from ",robot.range_and_bearing[i].data[1])
            end
        end
    end
end
--------------------------------------------------------------------------------
--------------------------function dest_orient_determine------------------------
--------------------------------------------------------------------------------
function dest_orient_determine()
  log(object)
  if object == "large_box" then --large_box to north
    orient_msg = compose(nearest.data[1],7,empty_skill)
    robot.range_and_bearing.set_data(orient_msg)
    state = "go_home"
  elseif object == "large_disc" then --go south
    orient_msg = compose(nearest.data[1],9,empty_skill)
    robot.range_and_bearing.set_data(orient_msg)
    state = "go_home"
  elseif object == "small_disc" then --go west
    orient_msg = compose(nearest.data[1],8,empty_skill)
    robot.range_and_bearing.set_data(orient_msg)
    state = "go_home"
  elseif object == "small_box" then --go east
    orient_msg = compose(nearest.data[1],10,empty_skill)
    robot.range_and_bearing.set_data(orient_msg)
    state = "go_home"
  end
end
-----------------------------function go home-----------------------------------
--------------------------------------------------------------------------------

function go_home()
    sign = robot.positioning.orientation.axis.z
    angle = robot.positioning.orientation.angle
    if orient_msg[3] == 7 then
        if (sign == 1 and angle < 0.1) or (sing == -1 and angle > 6.1) then
            go_ahead = 1
        else
            robot.wheels.set_velocity(-1,1)
        end
    elseif orient_msg[3] == 8 then
        if (sign == 1 and angle > 1.4 and angle < 1.6) or (sign == -1 and angle > 4.6 and angle < 4.7) then
            go_ahead = 1
        else
            robot.wheels.set_velocity(-1,1)
        end
    elseif orient_msg[3] == 9 then
        if angle > 3 and angle < 3.2 then
            go_ahead = 1
        else
            robot.wheels.set_velocity(-1,1)
        end
    elseif orient_msg[3] == 10 then
        if (sign == 1 and angle > 4.6 and angle < 4.7) or (sign == -1 and angle > 1.4 and angle < 1.6) then
            go_ahead = 1
        else
            robot.wheels.set_velocity(1,-1)
        end
    end
    if #robot.range_and_bearing then
        for i = 1, #robot.range_and_bearing do
            if robot.range_and_bearing[i].data[2] == self_addr and
                robot.range_and_bearing[i].data[3] == 11 then

                lets_go_msg = compose(nearest.data[1],12,empty_skill)
                robot.range_and_bearing.set_data(lets_go_msg)
                go_ahead = 0
                state = "home"

            end
        end
    end
end
-------------------------------function home------------------------------------
--------------------------------------------------------------------------------
function home()
    robot.wheels.set_velocity(10,10)
    --log(robot.motor_ground[1].value)
    ground = robot.motor_ground[1].value +
             robot.motor_ground[2].value +
             robot.motor_ground[3].value +
             robot.motor_ground[4].value
    --log(ground)
    if ground <= 0.25 then
        --log(ground)
        --log(count_time)
        count_time = count_time + 1
        if count_time >= 20 then
            count_time = 0
            robot.gripper.unlock()
            robot.turret.set_position_control_mode()
            state = "search"
            int_state ="listening"
        end
    end
end
--------------------------------------------------------------------------------
---------------internal state functions-----------------------------------------
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
function responding_to_braodcast()
    send_ack = compose(caller.data[1],2,my_skill)
    robot.range_and_bearing.set_data(send_ack)
    int_state = "waiting_for_final_call"
end
--------------------------------------------------------------------------------
-----------------------Function waiting_for_final_call--------------------------
--------------------------------------------------------------------------------
function waiting_for_final_call()
    if #robot.range_and_bearing > 0 then
        for i =1, #robot.range_and_bearing do
            if robot.range_and_bearing[i].data[2] == self_addr
                and robot.range_and_bearing[i].data[3] == 3 then
                caller = robot.range_and_bearing[i]
                log (self_addr,"received",msg[3]," from ",robot.range_and_bearing[i].data[1])
                int_state = "give_final_ack"
            elseif robot.range_and_bearing[i].data[3] == 3
                and robot.range_and_bearing[i].data[2] ~= self_addr then
                int_state = "listening"
            end
        end
    end
end
--------------------------------------------------------------------------------
-------------------------function give_final_ack--------------------------------
--------------------------------------------------------------------------------
function give_final_ack()
    final_ack = compose(caller.data[1],4,empty_skill)
    robot.range_and_bearing.set_data(final_ack)
    log(self_addr," sending ",msg[4]," to ",caller.data[1])
    int_state = "orient"
end
--------------------------------------------------------------------------------
------------------------------function orient-----------------------------------
--------------------------------------------------------------------------------
function orient()
  if l ~= 0 and l ~= robot.wheels.left_velocity then
    int_state = "approach_caller"
  end
  if #robot.range_and_bearing > 0 then
    ang = 200
    for i = 1, #robot.range_and_bearing do
      if robot.range_and_bearing[i].data[2] == self_addr
        and robot.range_and_bearing[i].data[3] == 5 then
        ang = robot.range_and_bearing[i]
      end
    end
    if ang ~= 200 then
      if (ang.horizontal_bearing < 0.25 and
        ang.horizontal_bearing > -0.25) then
        int_state = "approach_caller"
      elseif ang.horizontal_bearing > 0 then
        robot.wheels.set_velocity(-1,1)
      elseif ang.horizontal_bearing < 0 then
        robot.wheels.set_velocity(1,-1)
      end
    end
  end
  l = robot.wheels.left_velocity
  r = robot.wheels.right_velocity
end
--------------------------------------------------------------------------------
-------------------------function approach_caller-------------------------------
--------------------------------------------------------------------------------
function approach_caller()
    sensingLeft =     robot.proximity[3].value +
                      robot.proximity[4].value +
                      robot.proximity[5].value +
                      robot.proximity[6].value +
                      robot.proximity[2].value +
                      robot.proximity[1].value

    sensingRight =    robot.proximity[19].value +
                      robot.proximity[20].value +
                      robot.proximity[21].value +
                      robot.proximity[22].value +
                      robot.proximity[24].value +
                      robot.proximity[23].value

    if #robot.range_and_bearing then

        for i = 1, #robot.range_and_bearing do

            if robot.range_and_bearing[i].data[2] == self_addr and
                robot.range_and_bearing[i].data[3] == 5 then

                if robot.range_and_bearing[i].range > 75 then

                    if sensingLeft ~= 0 then
                        robot.wheels.set_velocity(7,3)
                    elseif sensingRight ~= 0 then
                        robot.wheels.set_velocity(3,7)
                    elseif robot.range_and_bearing[i].horizontal_bearing > 0 and
                            robot.range_and_bearing[i].horizontal_bearing <= 0.5 then
                        robot.wheels.set_velocity(9,10)
                    elseif robot.range_and_bearing[i].horizontal_bearing >0.5 then
                        robot.wheels.set_velocity(3,7)
                    elseif robot.range_and_bearing[i].horizontal_bearing < 0 and
                            robot.range_and_bearing[i].horizontal_bearing >= -0.5 then
                        robot.wheels.set_velocity(10,9)
                    elseif robot.range_and_bearing[i].horizontal_bearing < -0.5 then
                        robot.wheels.set_velocity(7,3)
                    end
                elseif robot.range_and_bearing[i].range < 50 then

                    if #robot.colored_blob_omnidirectional_camera > 0 then
                        dist = 100
                        this_ang =  robot.colored_blob_omnidirectional_camera[1].angle
                        for i = 1, #robot.colored_blob_omnidirectional_camera do
                            --log(i)
                            if  (robot.colored_blob_omnidirectional_camera[i].color.red == 255 and
                                robot.colored_blob_omnidirectional_camera[i].color.green == 255 and
                                robot.colored_blob_omnidirectional_camera[i].color.blue == 255) then


                                dist = robot.colored_blob_omnidirectional_camera[i].distance
                                this_ang = robot.colored_blob_omnidirectional_camera[i].angle
                                --log(dist)
                                if dist > 30 and this_ang > 0 then
                                    robot.wheels.set_velocity(3,7)
                                elseif dist > 30 and this_ang < 0 then
                                    robot.wheels.set_velocity(7,3)
                                elseif dist < 30 and dist > 29 then
                                    robot.wheels.set_velocity(5 * (dist - 1), 5 * (dist - 1))
                                elseif dist < 29 and dist > .1 then
                                    int_state = "grab_robot"
                                end
                            elseif (robot.colored_blob_omnidirectional_camera[i].color.red == 165 and--large_disc
                                  robot.colored_blob_omnidirectional_camera[i].color.green == 42 and
                                  robot.colored_blob_omnidirectional_camera[i].color.blue == 42) or
                                  (robot.colored_blob_omnidirectional_camera[i].color.red == 160 and--small box
                                  robot.colored_blob_omnidirectional_camera[i].color.green == 32 and
                                  robot.colored_blob_omnidirectional_camera[i].color.blue == 240) then

                                    dist = robot.colored_blob_omnidirectional_camera[i].distance
                                    this_ang = robot.colored_blob_omnidirectional_camera[i].angle
                                    --log(dist)
                                    if dist > 40 and this_ang > 0 then
                                        robot.wheels.set_velocity(3,7)
                                    elseif dist > 40 and this_ang < 0 then
                                        robot.wheels.set_velocity(7,3)
                                    elseif dist < 40 and dist > 39 then
                                        robot.wheels.set_velocity(5 * (dist - 1), 5 * (dist - 1))
                                    elseif dist < 39 and dist > .1 then
                                        int_state = "grab_robot"
                                    end
                            elseif (robot.colored_blob_omnidirectional_camera[i].color.red == 255 and--large_box
                                    robot.colored_blob_omnidirectional_camera[i].color.green == 140 and
                                    robot.colored_blob_omnidirectional_camera[i].color.blue == 0) and
                                    robot.colored_blob_omnidirectional_camera[i].distance < dist then

                                      dist = robot.colored_blob_omnidirectional_camera[i].distance
                                      this_ang = robot.colored_blob_omnidirectional_camera[i].angle
                                      --log(dist)
                                      if dist > 50 and this_ang > 0 then
                                          robot.wheels.set_velocity(3,7)
                                      elseif dist > 50 and this_ang < 0 then
                                          robot.wheels.set_velocity(7,3)
                                      elseif dist < 50 and dist > 49 then
                                          robot.wheels.set_velocity(5 * (dist - 1), 5 * (dist - 1))
                                      elseif dist < 49 and dist > .1 then
                                          int_state = "grab_robot"
                                      end

                            end
                        end
                    end
                end
            end
        end
    end
end
--------------------------------------------------------------------------------
--------------------------function grab_robot-----------------------------------
--------------------------------------------------------------------------------
function grab_robot()
    grip_ang = 200
    x = robot.proximity[1]
    x.value = 0
    pos = 0
    for i = 1,24 do
        if robot.proximity[i].value >= x.value then
            x = robot.proximity[i]
            pos = i
        end
    end
    if x.value == 1 then
        grip_ang = x.angle
    elseif pos >= 1 and pos <= 12 then
        robot.wheels.set_velocity(0,0.75)
    elseif pos >= 13 and pos <= 24 then
        robot.wheels.set_velocity(0.75,0)
    end
    if grip_ang ~= 200 then
        --log(pos," angle: ",x.angle,grip_ang,"value: ",robot.proximity[pos].value)
        robot.wheels.set_velocity(0,0)
        robot.turret.set_rotation(grip_ang)
        robot.gripper.lock_negative()
        count_time = count_time + 1
    end
    if count_time == 50 then
        robot.gripper.lock_negative()
        robot.turret.set_passive_mode()
        count_time = 0
        grabbed_msg = compose(caller.data[1],6,empty_skill)
        robot.range_and_bearing.set_data(grabbed_msg)
        int_state = "final_orientation"
    end
end
--------------------------------------------------------------------------------
---------------------Function orientaion set------------------------------------
--------------------------------------------------------------------------------
function final_orientation()
    sign = robot.positioning.orientation.axis.z
    angle = robot.positioning.orientation.angle
    if #robot.range_and_bearing then
        for i = 1, #robot.range_and_bearing do

            if robot.range_and_bearing[i].data[2] == self_addr and
                robot.range_and_bearing[i].data[1] == caller.data[1] then

                    direction = robot.range_and_bearing[i].data[3]
            end

        end
    end
  --  log(direction)
    if direction == 7 then --go top/north
        if (sign == 1 and angle < 0.1) or
            (sing == -1 and angle > 6.1) then

            ready_to_go_msg = compose(caller.data[1],11,empty_skill)
            robot.range_and_bearing.set_data(ready_to_go_msg)
            direction = 0
            int_state = "home"
        else
            robot.turret.set_speed_control_mode()
            robot.turret.set_rotation_speed(-1.2)
            robot.wheels.set_velocity(-1,1)
        end

    elseif direction == 8 then --go left/west
        if (sign == 1 and angle > 1.4 and angle < 1.6) or
            (sign == -1 and angle > 4.6 and angle < 4.7) then

            ready_to_go_msg = compose(caller.data[1],11,empty_skill)
            robot.range_and_bearing.set_data(ready_to_go_msg)
            direction = 0
            int_state = "home"
        else
            robot.turret.set_speed_control_mode()
            robot.turret.set_rotation_speed(-1.2)
            robot.wheels.set_velocity(-1,1)
        end

    elseif direction == 9 then --go down/south
        if angle > 3 and angle < 3.2 then
            ready_to_go_msg = compose(caller.data[1],11,empty_skill)
            robot.range_and_bearing.set_data(ready_to_go_msg)
            direction = 0
            int_state = "home"
        else
            robot.turret.set_speed_control_mode()
            robot.turret.set_rotation_speed(-1.2)
            robot.wheels.set_velocity(-1,1)
        end

    elseif direction == 10 then --go right/west
        if (sign == 1 and angle > 4.6 and angle < 4.7) or
            (sign == -1 and angle > 1.4 and angle < 1.6) then
            ready_to_go_msg = compose(caller.data[1],11,empty_skill)
            robot.range_and_bearing.set_data(ready_to_go_msg)
            direction = 0
            int_state = "home"
        else
            robot.turret.set_speed_control_mode()
            robot.turret.set_rotation_speed(-1.2)
            robot.wheels.set_velocity(-1,1)
        end
    end
end
--------------------------------------------------------------------------------
