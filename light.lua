x = 4.5
function round(num, numDecimalPlaces)
  local mult = 10^(numDecimalPlaces or 0)
  return math.floor(num * mult + 0.5) / mult
end
while x >= -4.5 do
  x = round(x,2)
  print('<led offset="'..x..',-0.1,0.11" anchor="origin" color="green" />')
  x = x -0.05
end
