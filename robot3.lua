msg = {}
msg[1] = "Help"
msg[2] = "Willing"
msg[3] = "ACK_initiator"
msg[4] = "ACK_non-initator"
msg[5] = "beacon"
msg[6] = "grabbed road-blocker"
msg[7] = "go north"
msg[8] = "go west"
msg[9] = "go south"
msg[10] = "go east"
msg[11] = "home orientation set"
msg[12] = "let's go"
--------------------------------------------------------------------------------
cost_per_task = {}
turn_around_timer = 0
l = 0
r = 0
call_wait = 0
direction = 0
count_time = 0
go_ahead = 0
received_message = {}
--------------------------------------------------------------------------------
local m = require "matrix"
bat_total = 10000
bat_cur = 10000
grab_timer = 0
es = 0
object = "none"
--------------------------------------------------------------------------------
rs_small_box = m{{1,1,0,1,0,0}}
rs_large_box = m{{1,1,1,1,1,0}}
rs_small_disc = m{{1,0,0,0,0,1}}
rs_large_disc = m{{1,0,1,0,1,0}}
required_skills = m(1,6,0)
chk_skill = m(1,6,0)
empty_skill = m(1,8,0)
--------------------------------------------------------------------------------
my_skill = m{{0,0,0,1,0,1,60,20}}
--------------------------------------------------------------------------------
function round(num, numDecimalPlaces)
  local mult = 10^(numDecimalPlaces or 0)
  return math.floor(num * mult + 0.5) / mult
end
--------------------------------------------------------------------------------
function check_skill()
  if #robot.colored_blob_omnidirectional_camera ~= 0  then
      for i = 1,#robot.colored_blob_omnidirectional_camera do
          if (robot.colored_blob_omnidirectional_camera[i].color.red == 165 and
              robot.colored_blob_omnidirectional_camera[i].color.green == 42 and
              robot.colored_blob_omnidirectional_camera[i].color.blue == 42)  then
                  chk_skill = rs_large_disc
          elseif(robot.colored_blob_omnidirectional_camera[i].color.red == 255 and
              robot.colored_blob_omnidirectional_camera[i].color.green == 255 and
              robot.colored_blob_omnidirectional_camera[i].color.blue == 255) then
                  chk_skill =  rs_small_disc
          elseif(robot.colored_blob_omnidirectional_camera[i].color.red == 160 and
              robot.colored_blob_omnidirectional_camera[i].color.green == 32 and
              robot.colored_blob_omnidirectional_camera[i].color.blue == 240) then
                  chk_skill = rs_small_box
          elseif(robot.colored_blob_omnidirectional_camera[i].color.red == 255 and
              robot.colored_blob_omnidirectional_camera[i].color.green == 140 and
              robot.colored_blob_omnidirectional_camera[i].color.blue == 0) then
                  chk_skill = rs_small_box
          end
      end
  end
  chk_flag = 0
  for i = 1, 6 do
    if chk_skill[1][i] == my_skill[1][i] then
      check_flag = 1
    end
  end
  if #robot.colored_blob_omnidirectional_camera ~= 0  then
      for i = 1,#robot.colored_blob_omnidirectional_camera do
        if (robot.colored_blob_omnidirectional_camera[i].color.red == 0 and
            robot.colored_blob_omnidirectional_camera[i].color.green == 0 and
            robot.colored_blob_omnidirectional_camera[i].color.blue == 255) or
          (robot.colored_blob_omnidirectional_camera[i].color.red == 255 and
            robot.colored_blob_omnidirectional_camera[i].color.green == 0 and
            robot.colored_blob_omnidirectional_camera[i].color.blue == 255) or
          (robot.colored_blob_omnidirectional_camera[i].color.red == 255 and
            robot.colored_blob_omnidirectional_camera[i].color.green == 0 and
            robot.colored_blob_omnidirectional_camera[i].color.blue == 0) or
          (robot.colored_blob_omnidirectional_camera[i].color.red == 255 and
            robot.colored_blob_omnidirectional_camera[i].color.green == 255 and
            robot.colored_blob_omnidirectional_camera[i].color.blue == 0) or
            (robot.colored_blob_omnidirectional_camera[i].color.red == 0 and
            robot.colored_blob_omnidirectional_camera[i].color.green == 255 and
            robot.colored_blob_omnidirectional_camera[i].color.blue == 255) or
            (robot.colored_blob_omnidirectional_camera[i].color.red == 0 and
            robot.colored_blob_omnidirectional_camera[i].color.green == 255 and
            robot.colored_blob_omnidirectional_camera[i].color.blue == 0) then
              check_flag = 0
          end
        end
      end
  return(check_flag)
end
-----------------function compose()---------------------------------------------
function compose(to_addr,meaning,skill)
  message = {self_addr,to_addr,meaning}
  for i = 1, 8 do
    table.insert(message, skill[1][i])
  end
  return(message)
end
--------------------------------------------------------------------------------
function init()
  self_addr = addr(robot.id)
  log(robot.id," = ",id)
  state = "search"
  prev_state = "dummy"
  int_state = "listening"
  prev_int_state = "dummy"
  robot.colored_blob_omnidirectional_camera.enable()
end
--------------------------------------------------------------------------------
function step()
    --colors
--[[    if (bat_cur/bat_total) > 0.25 then
        robot.leds.set_all_colors(0,0,255)
    else
        robot.leds.set_all_colors(255,0,0)
    end]]--
    if state ~= prev_state then
        log(self_addr,"=",state)
    end
    prev_state = state
    if state == "search" then
      robot.leds.set_all_colors(0,0,0)
        search()
    elseif state == "leave_task" then
      robot.leds.set_all_colors(0,0,0)
      leave_task()
    elseif state == "choose" then
      robot.leds.set_all_colors(255,0,0)
        choose()
    elseif state == "approach" then
      robot.leds.set_all_colors(255,0,0)
        approach()
    elseif state == "grab" then
      robot.leds.set_all_colors(255,0,0)
      grab()
    elseif state == "decide" then
      robot.leds.set_all_colors(255,0,0)
        decide()
    elseif state == "call" then
      robot.leds.set_all_colors(255,0,0)
      call()
    elseif state == "specific_call" then
      robot.leds.set_all_colors(255,0,0)
      specific_call()
    elseif state == "waiting_for_final_ack" then
      robot.leds.set_all_colors(255,0,0)
      waiting_for_final_ack()
    elseif state == "waiting" then
      robot.leds.set_all_colors(255,0,0)
      waiting()
    elseif state == "determine_orient" then
      robot.leds.set_all_colors(255,0,0)
        determine_orient()
    elseif state == "go_home" then
      robot.leds.set_all_colors(255,0,0)
        go_home()
    elseif state == "home" then
      robot.leds.set_all_colors(255,0,0)
        home()
    elseif state == "dest_orient_determine" then
      robot.leds.set_all_colors(255,0,0)
      dest_orient_determine()
    end
end
--------------------------------------------------------------------------------
function reset()
end
function destroy()
    log('Energy spent by ',robot.id,' / ',id,' = ',es,'\n')
    print("cost_per_task by: ",robot.id)
    if #cost_per_task > 0 then
      for i = 1, #cost_per_task do
        print(cost_per_task[i])
      end
    end
end
--------------------------------------------------------------------------------
----------------------------------addr fn----------------------    log(self_addr," sending ",msg[2]," to ",caller.data[1])
-----------------
-----------A hash function that decides the address of the robot----------------
--------------------------------------------------------------------------------
function addr(s)
    i = 0
    id = 0
    for c in s:gmatch"." do
        id = id + (string.byte(c) * math.pow(2 , i))
        i = i + 1
    end
    id = math.fmod(id,251) + 1
    return id
end
--------------------------------------------------------------------------------
-----------------------------function search()----------------------------------
--------------------------------------------------------------------------------
function search()
    --bat_cur = bat_cur - 10
    if int_state ~= prev_int_state then
        log(self_addr,"=",int_state)
    end
    prev_int_state = int_state
    es = es + 10
    if int_state == "listening" then
      sensingLeft =     robot.proximity[3].value +
                        robot.proximity[4].value +
                        robot.proximity[5].value +
                        robot.proximity[6].value +
                        robot.proximity[2].value +
                        robot.proximity[1].value

      sensingRight =    robot.proximity[19].value +
                        robot.proximity[20].value +
                        robot.proximity[21].value +
                        robot.proximity[22].value +
                        robot.proximity[24].value +
                        robot.proximity[23].value
      --[[if bat_cur <= (30/100) * bat_total then
          state = "to_charge"
      end]]
      if sensingLeft ~= 0 then
            robot.wheels.set_velocity(7,3)
      elseif sensingRight ~= 0 then
            robot.wheels.set_velocity(3,7)
      else
            robot.wheels.set_velocity(10,10)
      end
      if #robot.colored_blob_omnidirectional_camera ~= 0  then
          for i = 1,#robot.colored_blob_omnidirectional_camera do
              check_flag = check_skill()
              if (robot.colored_blob_omnidirectional_camera[i].color.red == 165 and
                  robot.colored_blob_omnidirectional_camera[i].color.green == 42 and
                  robot.colored_blob_omnidirectional_camera[i].color.blue == 42) and check_flag == 1  then
                      state = "choose"
                      object = "large_disc"
                      log(object)
              elseif(robot.colored_blob_omnidirectional_camera[i].color.red == 255 and
                  robot.colored_blob_omnidirectional_camera[i].color.green == 255 and
                  robot.colored_blob_omnidirectional_camera[i].color.blue == 255) and check_flag == 1 then
                      state = "choose"
                      object = "small_disc"
                      log(object)
              elseif(robot.colored_blob_omnidirectional_camera[i].color.red == 160 and
                  robot.colored_blob_omnidirectional_camera[i].color.green == 32 and
                  robot.colored_blob_omnidirectional_camera[i].color.blue == 240) and check_flag == 1 then
                      state = "choose"
                      object = "small_box"
                      log(object)
              elseif(robot.colored_blob_omnidirectional_camera[i].color.red == 255 and
                  robot.colored_blob_omnidirectional_camera[i].color.green == 140 and
                  robot.colored_blob_omnidirectional_camera[i].color.blue == 0) and check_flag == 1 then
                      state = "choose"
                      object = "large_box"
                      log(object)
              end
          end
      end
      calls = {}
      if #robot.range_and_bearing > 0 then
          for i = 1,#robot.range_and_bearing do
              if robot.range_and_bearing[i].data[2] == 254 then
                  table.insert(calls, robot.range_and_bearing[i])
              end
          end
      end
      if #calls > 0 then
          distance = 10000
          caller = 0
          for i = 1, #calls do
              if calls[i].range < distance then
                  distance = calls[i].range
                  caller = calls[i]
              end
          end
      end
      if #calls > 0 and caller ~= 0 then
        int_state = "responding_to_braodcast"
      end
  elseif int_state == "responding_to_braodcast" then
      responding_to_braodcast()
  elseif int_state == "waiting_for_final_call" then
      waiting_for_final_call()
  elseif int_state == "give_final_ack" then
      give_final_ack()
  elseif int_state == "orient" then
      orient()
  elseif int_state == "approach_caller" then
      approach_caller()
  elseif int_state == "grab_robot" then
    robot.leds.set_all_colors(255,0,0)
      grab_robot()
  elseif int_state == "final_orientation" then
    robot.leds.set_all_colors(255,0,0)
      final_orientation()
  elseif int_state == "home" then
    robot.leds.set_all_colors(255,0,0)
      home()
  end
end
--------------------------------------------------------------------------------
--------------------------------to charge()-------------------------------------
--------------------------------------------------------------------------------
--[[function to_charge()
    if robot.positioning.position.x < 0 then -- the location we must go

    elseif robot.positioning.position.x >= 0 then

    end

end--]]
--[[----------------------------------------------------------------------------
-----------------------------function charge()----------------------------------
--------------------------------------------------------------------------------
function to_charge()
    robot.wheels.set_velocity(0,0)
    bat_cur = bat_cur + 10
    if bat_cur >= (90/100)*bat_total then
        state = "search"
    end
end
--------------------------------------------------------------------------------
]]--
----------------------------function choose()-----------------------------------
function choose()
  if #robot.colored_blob_omnidirectional_camera == 0 then
      state = "search"
      --log(1)
  else
      --log(2)
      robot.wheels.set_velocity(0,0)
      closest = robot.colored_blob_omnidirectional_camera[1]
      dist = robot.colored_blob_omnidirectional_camera[1].distance
      ang =  robot.colored_blob_omnidirectional_camera[1].angle
      for i = 1, #robot.colored_blob_omnidirectional_camera do

          if (robot.colored_blob_omnidirectional_camera[i].color.red == 165 and
              robot.colored_blob_omnidirectional_camera[i].color.green == 42 and
              robot.colored_blob_omnidirectional_camera[i].color.blue == 42) or
              (robot.colored_blob_omnidirectional_camera[i].color.red == 255 and
               robot.colored_blob_omnidirectional_camera[i].color.green == 255 and
               robot.colored_blob_omnidirectional_camera[i].color.blue == 255) or
               (robot.colored_blob_omnidirectional_camera[i].color.red == 160 and
                robot.colored_blob_omnidirectional_camera[i].color.green == 32 and
                robot.colored_blob_omnidirectional_camera[i].color.blue == 240) or
                (robot.colored_blob_omnidirectional_camera[i].color.red == 255 and
                robot.colored_blob_omnidirectional_camera[i].color.green == 140 and
                robot.colored_blob_omnidirectional_camera[i].color.blue == 0)then
              closest = robot.colored_blob_omnidirectional_camera[i]
              dist = closest.distance
              ang = closest.angle
          end
      end
      if ang > 0.1 then
          robot.wheels.set_velocity(-1,1)
      elseif ang < -0.1 then
          robot.wheels.set_velocity(1,-1)
      elseif ang >= -0.1 and ang <= 0.1 then
          state = "approach"
      end
  end
end
---------------------------function approach()----------------------------------
function approach()
    if #robot.colored_blob_omnidirectional_camera > 0 then
        x = 0
        for i = 1, 24 do --some modification must be done here as we need not check
                         --all proximity sensors then ones located in front shall do
            if x < robot.proximity[i].value then
                x = robot.proximity[i].value
            end
        end
-------trying to keep the orientation while approaching the obstacle------------
        dist = robot.colored_blob_omnidirectional_camera[1].distance
        ang =  robot.colored_blob_omnidirectional_camera[1].angle

        for i = 1, #robot.colored_blob_omnidirectional_camera do
            if dist > robot.colored_blob_omnidirectional_camera[i].distance and
                (robot.colored_blob_omnidirectional_camera[i].color.red == 255 or
                robot.colored_blob_omnidirectional_camera[i].color.blue == 255) then

                dist = robot.colored_blob_omnidirectional_camera[i].distance
                ang = robot.colored_blob_omnidirectional_camera[i].angle

            end
        end
        if ang > 0 then
            robot.wheels.set_velocity(5,6)
        end
        if ang < 0 then
            robot.wheels.set_velocity(6,5)
        end
        if ang == 0 then
            robot.wheels.set_velocity(5,5)
        end
-------------trying to slow down when reaching near the obstacle----------------
        if x >= 0.5 then
            robot.wheels.set_velocity((1 - x) * 10, (1 - x) * 10)
        end
        if x >= 0.9 then
            robot.wheels.set_velocity(0,0)
            state = "grab"
        end
    else
        state = "search"
    end
end
---------------grab function----
function grab()
  --log(grab_timer)
  grab_timer = grab_timer + 1
    grip_ang = 200
    x = robot.proximity[1]
    x.value = 0
    pos = 0
    for i = 1,24 do
        if robot.proximity[i].value >= x.value then
            x = robot.proximity[i]
            pos = i
        end
    end
    if x.value >= 0.92 then
        grip_ang = x.angle
    elseif pos >= 1 and pos <= 12 then
        robot.wheels.set_velocity(0,0.75)
    elseif pos >= 13 and pos <= 24 then
        robot.wheels.set_velocity(0.75,0)
    end
    if grip_ang ~= 200 then
        --log(pos," angle: ",x.angle,grip_ang,"value: ",robot.proximity[pos].value)
        robot.wheels.set_velocity(0,0)
        robot.turret.set_rotation(grip_ang)
        robot.gripper.lock_negative()
        count_time = count_time + 1
    end
    if grab_timer >= 50 then
        robot.gripper.lock_negative()
        robot.turret.set_passive_mode()
        grab_timer = 0
        state = "decide"
    end
end
----------------------------function decide()-----------------------------------
function decide()
  log(object)
  if object == "small_box" then
    required_skills = rs_small_box
  elseif object == "large_box" then
    required_skills = rs_large_box
  elseif object == "small_disc" then
    required_skills = rs_small_disc
  elseif object == "large_disc" then
    required_skills = rs_large_box
  else
    state = "search"
  end
  print(required_skills)
  for i = 1, 6 do
    if my_skill[1][i] == 1 and required_skills[1][i] == 1 then
      required_skills[1][i] = required_skills[1][i] - 1
    end
  end
  dummy_alpha_beta = m(1,2,0)
  required_skills = m.concath(required_skills,dummy_alpha_beta)
  print(required_skills)
  state = "call"
end
--------------------------------------------------------------------------------
function call()
  add_skills = compose(254,1,required_skills)
  robot.range_and_bearing.set_data(add_skills)
  distance = 10000
  nearest = 0
  available_robots = {}
  call_wait = call_wait + 1
  if #robot.range_and_bearing > 0 then
      for i = 1, #robot.range_and_bearing do
          if robot.range_and_bearing[i].data[2] == self_addr and
              robot.range_and_bearing[i].data[3] == 2 then
              if #available_robots > 0 then
                check_robo = 0
                for j = 1, #available_robots do
                  if available_robots[j].data[1] == robot.range_and_bearing[i].data[1] then
                    check_robo = 1
                  end
                end
                if check_robo == 0 then
                  table.insert(available_robots,robot.range_and_bearing[i])
                end
              else
                table.insert(available_robots,robot.range_and_bearing[i])
              end
          end
      end
  end
  for i = 1, #available_robots do
    log(available_robots[i].data[1])
  end
  if call_wait >= 10 and #available_robots > 0 then
      call_wait = 0
      state = "specific_call"
    elseif call_wait >= 10 and #available_robots == 0 then
      call_wait = 0
      robot.gripper.unlock()
      state = "leave_task"
  end
end
--------------------------------------------------------------------------------
function leave_task()
  if turn_around_timer < 35 then
    robot.wheels.set_velocity(5,-5)
    turn_around_timer = turn_around_timer + 1
  else
    robot.wheels.set_velocity(10,10)
  end
  leave_task_flag = leave_check()
  if leave_task_flag == 1 then
    state = "search"
    turn_around_timer = 0
  end
end
--------------------------------------------------------------------------------
function specific_call()
    selection()
    specific_msg = multi_compose(3)
    robot.range_and_bearing.set_data(specific_msg)
    state = "waiting_for_final_ack"
end
---------------------Function waiting_for_final_ack-----------------------------
--------------------------------------------------------------------------------
function waiting_for_final_ack()
    --[[if #robot.range_and_bearing > 0 then
        for i = 1, #robot.range_and_bearing do
            if robot.range_and_bearing[i].data[3] == 4 then
                log(self_addr,"received final ack from",robot.range_and_bearing[i].data[1])
                state = "waiting"
            end
        end
    end]]--
    receive(4)
    if #received_message == #called_robots then
      state = "waiting"
      flush(received_message)
    end
end
------------------------------Function waiting----------------------------------
--------------------------------------------------------------------------------
function waiting()
    robot.wheels.set_velocity(0,0)
    beacon = compose(255,5,empty_skill)
    robot.range_and_bearing.set_data(beacon)
    --[[if #robot.range_and_bearing > 0 then

        for i = 1,#robot.range_and_bearing do

            if robot.range_and_bearing[i].data[2] == self_addr and
                robot.range_and_bearing[i].data[3] == 6 then
                state = "dest_orient_determine"
                log (self_addr," received ",msg[robot.range_and_bearing[i].data[3]," from ",robot.range_and_bearing[i].data[1])
            end
        end
    end]]
    if #robot.range_and_bearing >= 0 then
      receive(6)
      if #received_message == #called_robots then
        state = "dest_orient_determine"
        flush(received_message)
      end
    end
end
--------------------------------------------------------------------------------
--------------------------function dest_orient_determine------------------------
--------------------------------------------------------------------------------
function dest_orient_determine()
  log(object)
  if object == "large_box" then --large_box to north
    orient_msg = compose(255,7,empty_skill)
    robot.range_and_bearing.set_data(orient_msg)
    state = "go_home"
  elseif object == "large_disc" then --go south
    orient_msg = compose(255,9,empty_skill)
    robot.range_and_bearing.set_data(orient_msg)
    state = "go_home"
  elseif object == "small_disc" then --go west
    orient_msg = compose(255,8,empty_skill)
    robot.range_and_bearing.set_data(orient_msg)
    state = "go_home"
  elseif object == "small_box" then --go east
    orient_msg = compose(255,10,empty_skill)
    robot.range_and_bearing.set_data(orient_msg)
    state = "go_home"
  end
end
-----------------------------function go home-----------------------------------
--------------------------------------------------------------------------------

function go_home()
    sign = robot.positioning.orientation.axis.z
    angle = robot.positioning.orientation.angle
    if orient_msg[3] == 7 then
        if (sign == 1 and angle < 0.1) or (sing == -1 and angle > 6.1) then
            go_ahead = 1
        else
            robot.wheels.set_velocity(-1,1)
        end
    elseif orient_msg[3] == 8 then
        if (sign == 1 and angle > 1.4 and angle < 1.6) or (sign == -1 and angle > 4.6 and angle < 4.7) then
            go_ahead = 1
        else
            robot.wheels.set_velocity(-1,1)
        end
    elseif orient_msg[3] == 9 then
        if angle > 3 and angle < 3.2 then
            go_ahead = 1
        else
            robot.wheels.set_velocity(-1,1)
        end
    elseif orient_msg[3] == 10 then
        if (sign == 1 and angle > 4.6 and angle < 4.7) or (sign == -1 and angle > 1.4 and angle < 1.6) then
            go_ahead = 1
        else
            robot.wheels.set_velocity(1,-1)
        end
    end
    --[[if #robot.range_and_bearing then
        for i = 1, #robot.range_and_bearing do
            if robot.range_and_bearing[i].data[2] == self_addr and
                robot.range_and_bearing[i].data[3] == 11 then

                lets_go_msg = compose(255,12,empty_skill)
                robot.range_and_bearing.set_data(lets_go_msg)
                go_ahead = 0
                state = "home"

            end
        end
    end]]
    if #robot.range_and_bearing > 0 then
      receive(11)
      if #received_message == #called_robots then
        lets_go_msg = compose(255,12,empty_skill)
        robot.range_and_bearing.set_data(lets_go_msg)
        go_ahead = 0
        state ="home"
        flush(received_message)
      end
    end
end
-------------------------------function home------------------------------------
--------------------------------------------------------------------------------
function home()
    robot.wheels.set_velocity(10,10)
    --log(robot.motor_ground[1].value)
    ground = robot.motor_ground[1].value +
             robot.motor_ground[2].value +
             robot.motor_ground[3].value +
             robot.motor_ground[4].value
    --log(ground)
    if ground <= 0.25 then
        --log(ground)
        --log(count_time)
        count_time = count_time + 1
        if count_time >= 20 then
            count_time = 0
            robot.gripper.unlock()
            robot.turret.set_position_control_mode()
            state = "search"
            int_state ="listening"
        end
    end
end
--------------------------------------------------------------------------------
---------------internal state functions-----------------------------------------
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
function responding_to_braodcast()
    send_ack = compose(caller.data[1],2,my_skill)
    robot.range_and_bearing.set_data(send_ack)
    int_state = "waiting_for_final_call"
end
--------------------------------------------------------------------------------
-----------------------Function waiting_for_final_call--------------------------
--------------------------------------------------------------------------------
function waiting_for_final_call()
    if #robot.range_and_bearing > 0 then
        --[[for i =1, #robot.range_and_bearing do
            if robot.range_and_bearing[i].data[2] == self_addr
                and robot.range_and_bearing[i].data[3] == 3 then
                caller = robot.range_and_bearing[i]
                log (self_addr,"received",msg[3]," from ",robot.range_and_bearing[i].data[1])
                int_state = "give_final_ack"
            elseif robot.range_and_bearing[i].data[3] == 3
                and robot.range_and_bearing[i].data[2] ~= self_addr then
                int_state = "listening"
            end
        end]]--
        for i = 1, #robot.range_and_bearing do
          if robot.range_and_bearing[i].data[3] == 3 then
            caller = robot.range_and_bearing[i]
          end
        end
        if caller ~= 0 then
          flag_waiting_ack = 0
          for i = 4, 11 do
            if caller.data[i] == self_addr then
              flag_waiting_ack = 1
            end
          end
        end
        if flag_waiting_ack == 0 then
          int_state = "listening"
        elseif flag_waiting_ack == 1 then
          int_state = "give_final_ack"
        end
    end
end
--------------------------------------------------------------------------------
-------------------------function give_final_ack--------------------------------
--------------------------------------------------------------------------------
function give_final_ack()
    final_ack = compose(caller.data[1],4,empty_skill)
    robot.range_and_bearing.set_data(final_ack)
    log(self_addr," sending ",msg[4]," to ",caller.data[1])
    int_state = "orient"
end
--------------------------------------------------------------------------------
------------------------------function orient-----------------------------------
--------------------------------------------------------------------------------
function orient()
  if #robot.range_and_bearing > 0 then
    for i = 1, #robot.range_and_bearing do
      if robot.range_and_bearing[i].data[1] == caller.data[1] and
      robot.range_and_bearing[i].data[3] == 5 then
        if robot.range_and_bearing[i].horizontal_bearing < 0.25
        and robot.range_and_bearing[i].horizontal_bearing > -0.25 then
          int_state = "approach_caller"
        elseif robot.range_and_bearing[i].horizontal_bearing > 0 then
          robot.wheels.set_velocity(-1,1)
        elseif robot.range_and_bearing[i].horizontal_bearing < 0 then
          robot.wheels.set_velocity(1,-1)
        end
      end
    end
  end
end
--------------------------------------------------------------------------------
-------------------------function approach_caller-------------------------------
--------------------------------------------------------------------------------
function approach_caller()
    sensingLeft =     robot.proximity[3].value +
                      robot.proximity[4].value +
                      robot.proximity[5].value +
                      robot.proximity[6].value +
                      robot.proximity[2].value +
                      robot.proximity[1].value

    sensingRight =    robot.proximity[19].value +
                      robot.proximity[20].value +
                      robot.proximity[21].value +
                      robot.proximity[22].value +
                      robot.proximity[24].value +
                      robot.proximity[23].value

    if #robot.range_and_bearing then

        for i = 1, #robot.range_and_bearing do

            if robot.range_and_bearing[i].data[2] == 255 and
                robot.range_and_bearing[i].data[1] == caller.data[1] and
                robot.range_and_bearing[i].data[3] == 5 then

                if robot.range_and_bearing[i].range > 75 then

                    if sensingLeft ~= 0 then
                        robot.wheels.set_velocity(7,3)
                    elseif sensingRight ~= 0 then
                        robot.wheels.set_velocity(3,7)
                    elseif robot.range_and_bearing[i].horizontal_bearing > 0 and
                            robot.range_and_bearing[i].horizontal_bearing <= 0.5 then
                        robot.wheels.set_velocity(9,10)
                    elseif robot.range_and_bearing[i].horizontal_bearing >0.5 then
                        robot.wheels.set_velocity(3,7)
                    elseif robot.range_and_bearing[i].horizontal_bearing < 0 and
                            robot.range_and_bearing[i].horizontal_bearing >= -0.5 then
                        robot.wheels.set_velocity(10,9)
                    elseif robot.range_and_bearing[i].horizontal_bearing < -0.5 then
                        robot.wheels.set_velocity(7,3)
                    end
                elseif robot.range_and_bearing[i].range < 75 then

                    if #robot.colored_blob_omnidirectional_camera > 0 then
                        dist = 100
                        this_ang =  robot.colored_blob_omnidirectional_camera[1].angle
                        for i = 1, #robot.colored_blob_omnidirectional_camera do
                            --log(i)
                            if  (robot.colored_blob_omnidirectional_camera[i].color.red == 255 and
                                robot.colored_blob_omnidirectional_camera[i].color.green == 255 and
                                robot.colored_blob_omnidirectional_camera[i].color.blue == 255) then


                                dist = robot.colored_blob_omnidirectional_camera[i].distance
                                this_ang = robot.colored_blob_omnidirectional_camera[i].angle
                                --log(dist)
                                if dist > 30 and this_ang > 0 then
                                    robot.wheels.set_velocity(3,7)
                                elseif dist > 30 and this_ang < 0 then
                                    robot.wheels.set_velocity(7,3)
                                elseif dist < 30 and dist > 29 then
                                    robot.wheels.set_velocity(5 * (dist - 1), 5 * (dist - 1))
                                elseif dist < 29 and dist > .1 then
                                    int_state = "grab_robot"
                                end
                            elseif (robot.colored_blob_omnidirectional_camera[i].color.red == 165 and--large_disc
                                  robot.colored_blob_omnidirectional_camera[i].color.green == 42 and
                                  robot.colored_blob_omnidirectional_camera[i].color.blue == 42) or
                                  (robot.colored_blob_omnidirectional_camera[i].color.red == 160 and--small box
                                  robot.colored_blob_omnidirectional_camera[i].color.green == 32 and
                                  robot.colored_blob_omnidirectional_camera[i].color.blue == 240) then

                                    dist = robot.colored_blob_omnidirectional_camera[i].distance
                                    this_ang = robot.colored_blob_omnidirectional_camera[i].angle
                                    --log(dist)
                                    if dist > 40 and this_ang > 0 then
                                        robot.wheels.set_velocity(3,7)
                                    elseif dist > 40 and this_ang < 0 then
                                        robot.wheels.set_velocity(7,3)
                                    elseif dist < 40 and dist > 39 then
                                        robot.wheels.set_velocity(5 * (dist - 1), 5 * (dist - 1))
                                    elseif dist < 39 and dist > .1 then
                                        int_state = "grab_robot"
                                    end
                            elseif (robot.colored_blob_omnidirectional_camera[i].color.red == 255 and--large_box
                                    robot.colored_blob_omnidirectional_camera[i].color.green == 140 and
                                    robot.colored_blob_omnidirectional_camera[i].color.blue == 0) and
                                    robot.colored_blob_omnidirectional_camera[i].distance < dist then

                                      dist = robot.colored_blob_omnidirectional_camera[i].distance
                                      this_ang = robot.colored_blob_omnidirectional_camera[i].angle
                                      --log(dist)
                                      if dist > 50 and this_ang > 0 then
                                          robot.wheels.set_velocity(3,7)
                                      elseif dist > 50 and this_ang < 0 then
                                          robot.wheels.set_velocity(7,3)
                                      elseif dist < 50 and dist > 49 then
                                          robot.wheels.set_velocity(5 * (dist - 1), 5 * (dist - 1))
                                      elseif dist < 49 and dist > .1 then
                                          int_state = "grab_robot"
                                      end

                            end
                        end
                    end
                end
            end
        end
    end
end
--------------------------------------------------------------------------------
--------------------------function grab_robot-----------------------------------
--------------------------------------------------------------------------------
function grab_robot()
    grip_ang = 200
    x = robot.proximity[1]
    x.value = 0
    pos = 0
    for i = 1,24 do
        if robot.proximity[i].value >= x.value then
            x = robot.proximity[i]
            pos = i
        end
    end
    if x.value == 1 then
        grip_ang = x.angle
    elseif pos >= 1 and pos <= 12 then
        robot.wheels.set_velocity(0,0.75)
    elseif pos >= 13 and pos <= 24 then
        robot.wheels.set_velocity(0.75,0)
    end
    if grip_ang ~= 200 then
        --log(pos," angle: ",x.angle,grip_ang,"value: ",robot.proximity[pos].value)
        robot.wheels.set_velocity(0,0)
        robot.turret.set_rotation(grip_ang)
        robot.gripper.lock_negative()
        count_time = count_time + 1
    end
    if count_time == 50 then
        robot.gripper.lock_negative()
        robot.turret.set_passive_mode()
        count_time = 0
        grabbed_msg = compose(caller.data[1],6,empty_skill)
        robot.range_and_bearing.set_data(grabbed_msg)
        int_state = "final_orientation"
    end
end
--------------------------------------------------------------------------------
---------------------Function orientaion set------------------------------------
--------------------------------------------------------------------------------
function final_orientation()
    sign = robot.positioning.orientation.axis.z
    angle = robot.positioning.orientation.angle
    if #robot.range_and_bearing then
        for i = 1, #robot.range_and_bearing do

            if robot.range_and_bearing[i].data[2] == 255 and
              robot.range_and_bearing[i].data[1] == caller.data[1]
              and(robot.range_and_bearing[i].data[3] == 7
              or robot.range_and_bearing[i].data[3] == 8
              or robot.range_and_bearing[i].data[3] == 9
              or robot.range_and_bearing[i].data[3] == 10) then

                    direction = robot.range_and_bearing[i].data[3]
            end

        end
    end
  --  log(direction)
    if direction == 7 then --go top/north
        if (sign == 1 and angle < 0.1) or
            (sing == -1 and angle > 6.1) then

            ready_to_go_msg = compose(caller.data[1],11,empty_skill)
            robot.range_and_bearing.set_data(ready_to_go_msg)
            direction = 0
            --int_state = "home"
        else
            robot.turret.set_speed_control_mode()
            robot.turret.set_rotation_speed(-1.2)
            robot.wheels.set_velocity(-1,1)
        end

    elseif direction == 8 then --go left/west
        if (sign == 1 and angle > 1.4 and angle < 1.6) or
            (sign == -1 and angle > 4.6 and angle < 4.7) then

            ready_to_go_msg = compose(caller.data[1],11,empty_skill)
            robot.range_and_bearing.set_data(ready_to_go_msg)
            direction = 0
            --int_state = "home"
        else
            robot.turret.set_speed_control_mode()
            robot.turret.set_rotation_speed(-1.2)
            robot.wheels.set_velocity(-1,1)
        end

    elseif direction == 9 then --go down/south
        if angle > 3 and angle < 3.2 then
            ready_to_go_msg = compose(caller.data[1],11,empty_skill)
            robot.range_and_bearing.set_data(ready_to_go_msg)
            direction = 0
            --int_state = "home"
        else
            robot.turret.set_speed_control_mode()
            robot.turret.set_rotation_speed(-1.2)
            robot.wheels.set_velocity(-1,1)
        end

    elseif direction == 10 then --go right/west
        if (sign == 1 and angle > 4.6 and angle < 4.7) or
            (sign == -1 and angle > 1.4 and angle < 1.6) then
            ready_to_go_msg = compose(caller.data[1],11,empty_skill)
            robot.range_and_bearing.set_data(ready_to_go_msg)
            direction = 0
            --int_state = "home"
        else
            robot.turret.set_speed_control_mode()
            robot.turret.set_rotation_speed(-1.2)
            robot.wheels.set_velocity(-1,1)
        end
    end
    if #robot.range_and_bearing > 0 then
      for i = 1, #robot.range_and_bearing do
        if robot.range_and_bearing[i].data[1] == caller.data[1]
        and robot.range_and_bearing[i].data[2] == 255
        and robot.range_and_bearing[i].data[3] == 12 then
          int_state = "home"
        end
      end
    end
end
--------------------------------------------------------------------------------
function selection()
  log("selection")
  skill_matrix = m(#available_robots,7,0)
  for i = 1, #available_robots do
    skill_matrix[i][1] = available_robots[i].data[1]
    for j = 2, 7 do
      skill_matrix[i][j] = available_robots[i].data[j+2]
    end
  end
  print("skill_matrix")
  print(skill_matrix)
  cost_matrix = m(#available_robots,7,0)
  cost_matrix = skill_matrix
  for i = 1, #available_robots do
    for j = 2, 7 do
      if cost_matrix[i][j] == 0 then
        cost_matrix[i][j] = -1
      end
    end
  end
  print("cost_matrix")
  print(cost_matrix)
  for i = 1, #available_robots do
    for j = 2, 7 do
      if cost_matrix[i][j] == 1 then
        cost_matrix[i][j] = available_robots[i].data[11] + (available_robots[i].data[10] / 100) * (available_robots[i].range)
        cost_matrix[i][j] = round(cost_matrix[i][j],2)
      end
    end
  end
  print("updated cost_matrix")
  print(cost_matrix)
  minimum_matrix = m(1,6,0)
  min_costs = m(1,6,0)
  min_costing_robots = m(1,6,0)
  for j = 2, 7 do
    min = 99999
    for i = 1, #available_robots do
      if cost_matrix[i][j] < min and cost_matrix[i][j] > 0 then
        min = cost_matrix[i][j]
        p = i
      end
    end
    if min ~= 99999 then
      min_costs[1][j-1] = min
      min_costing_robots[1][j-1] = cost_matrix[p][1]
    elseif min == 99999 then
      min_costs[1][j-1] = -1
      min_costing_robots[1][j-1] = -1
    end
  end
  print("min_costs")
  print(min_costs)
  print("min_costing_robots")
  print(min_costing_robots)
  for i = 1, 6 do
    min_costing_robots[1][i] = min_costing_robots[1][i] * required_skills[1][i]
  end
  print("needed robots")
  print(min_costing_robots)
  called_robots = {}
  log("available_robots", #available_robots)
  for i = 1, 6 do
    --log("ins called")
    ins(min_costing_robots[1][i])
  end
  log("called.no: ", #called_robots)
  add_cost(min_costs)
  --[[for i = 1, #called_robots do
    log(called_robots[i].data[1])
  end
  log("selection_done")]]
end
--------------------------------------------------------------------------------
function ins(n)
  --log(1)
  if #called_robots == 0 then
    --log(2)
    for i = 1, #available_robots do
      if available_robots[i].data[1] == n then
        table.insert(called_robots,available_robots[i])
      end
    end
  else
    ins_flag = 0
    for i = 1, #called_robots do
      if n == called_robots[i].data[1] then
        ins_flag = 1
      end
    end
    if ins_flag == 0 then
      for i = 1, #available_robots do
        if available_robots[i].data[1] == n then
          table.insert(called_robots,available_robots[i])
        end
      end
    end
  end
end
--------------------------------------------------------------------------------
function multi_compose(meaning)
  new_message = {}
  new_message[1] = self_addr
  new_message[2] = 255
  new_message[3] = meaning
  for i = 1, 8 do
    if called_robots[i] ~= nil then
      table.insert(new_message,called_robots[i].data[1])
    else
      table.insert(new_message,0)
    end
  end
  print("new_message")
  for i = 1, 11 do
    print(new_message[i])
  end
  return(new_message)
end
--------------------------------------------------------------------------------
function receive(r)
  if #received_message == 0 then
    for i = 1, #robot.range_and_bearing do
      if robot.range_and_bearing[i].data[2] == self_addr
        and robot.range_and_bearing[i].data[3] == r then

        table.insert(received_message, robot.range_and_bearing[i])
      end
    end
  elseif #received_message > 0 then
    for i = 1, #robot.range_and_bearing do
      if robot.range_and_bearing[i].data[2] == self_addr
          and robot.range_and_bearing[i].data[3] == r then
        receive_flag = 0
        for j = 1, #received_message do
          if robot.range_and_bearing[i].data[1] == received_message[j].data[1] then
            receive_flag = 1
          end
        end
        if receive_flag == 0 then
          table.insert(received_message, robot.range_and_bearing[i])
        end
      end
    end
  end
end
--------------------------------------------------------------------------------
function flush(t)
  while #t ~= 0 do rawset(t, #t, nil) end
end
--------------------------------------------------------------------------------
function add_cost(mi)
  indiv = {}
  for i = 1, #mi[1] do
    if #indiv == 0 then
      table.insert(indiv, mi[1][i])
    else
      add_cost_flag = 0
      for j = 1, #indiv do
        if indiv[j] == mi[1][i] then
          add_cost_flag = 1
        end
      end
      if add_cost_flag == 0 then
        table.insert(indiv, mi[1][i])
      end
    end
  end
  task_cost = 0
  if #indiv > 0 then
    for i = 1, #indiv do
      if indiv[i] ~= -1 then
        task_cost = task_cost + indiv[i]
      end
    end
  end
  print("task_cost")
  print(task_cost)
  scost = tostring(task_cost)
  scost = object.." :"..scost
  table.insert(cost_per_task,scost)
end
--------------------------------------------------------------------------------
function leave_check()
  leave_check_flag = 1
  if #robot.colored_blob_omnidirectional_camera == 0 then
    return(1)
  else
    for i = 1, #robot.colored_blob_omnidirectional_camera do
      if object == "large_disc" then
        if (robot.colored_blob_omnidirectional_camera[i].color.red == 165
        and robot.colored_blob_omnidirectional_camera[i].color.green == 42
        and robot.colored_blob_omnidirectional_camera[i].color.blue == 42) then
          leave_check_flag = 0
        end
      elseif object == "small_disc" then
        if (robot.colored_blob_omnidirectional_camera[i].color.red == 255
        and robot.colored_blob_omnidirectional_camera[i].color.green == 255
        and robot.colored_blob_omnidirectional_camera[i].color.blue == 255) then
          leave_check_flag = 0
        end
      elseif object == "small_box" then
        if(robot.colored_blob_omnidirectional_camera[i].color.red == 160
        and robot.colored_blob_omnidirectional_camera[i].color.green == 32
        and robot.colored_blob_omnidirectional_camera[i].color.blue == 240) then
          leave_check_flag = 0
        end
      elseif object == "large_box" then
        if(robot.colored_blob_omnidirectional_camera[i].color.red == 255
        and robot.colored_blob_omnidirectional_camera[i].color.green == 140
        and robot.colored_blob_omnidirectional_camera[i].color.blue == 0) then
          leave_check_flag = 0
        end
      end
    end
    return(leave_check_flag)
  end
end
--------------------------------------------------------------------------------
